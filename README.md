Role Name
=========

A role that installs the Bluecloud uFish stack

Role Variables
--------------

The most important variables are listed below:

``` yaml
bc_ufish_compose_dir: '/srv/bc_ufish_stack'
bc_ufish_docker_stack_name: 'bluecloud-ufish'
bc_ufish_docker_service_server_name: 'bc-ufish-server'
bc_ufish_docker_service_client_name: 'bc-ufish-client'
bc_ufish_docker_server_image: 'tommasinopierfrancesco/ufish-server'
bc_ufish_docker_client_image: 'tommasinopierfrancesco/ufish-client'
bc_ufish_docker_network: 'bc_ufish_net'
# IMPORTANT. Set it to True for the server that is going to host the DB
bc_ufish_behind_haproxy: True
bc_ufish_haproxy_public_net: 'haproxy-public'
# DB
bc_ufish_db_docker_host: 'localhost'
bc_ufish_db_host: 'ufish-mysql'
bc_ufish_db_port: 3306
bc_ufish_mysql_root_password: 'set it in a vault file'
bc_ufish_db_pwd: 'set it in a vault file'
bc_ufish_db_name: 'bc_ufish'
bc_ufish_db_user: 'bc_ufish_u'
bc_ufish_db_volume: 'bc_ufish_db_data'
bc_ufish_db_constraints: 'node.labels.ufish_db_data == bc_ufish'
```

Dependencies
------------

A docker swarm cluster is required

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
